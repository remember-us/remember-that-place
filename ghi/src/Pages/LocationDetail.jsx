import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

const API_HOST = import.meta.env.VITE_API_HOST

export default function LocationDetail() {
    const { id } = useParams()
    const [locData, setLocData] = useState({})
    const fetchData = async () => {
        const url = `${API_HOST}/locations/${id}`
        const request = await fetch(url)
        const body = await request.json()
        setLocData(body.Response)
    }

    useEffect(() => {
        fetchData()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <div className="container p-4">
            <div className="card-body  p-2">
                <h1>{locData.name}</h1>
                <img
                    width={600}
                    height={600}
                    style={{
                        objectFit: 'cover',
                        maxheight: '260vh',
                    }}
                    src={locData.image_url}
                    alt={locData.name}
                />
                <p>{locData.rating}</p>
                {locData.location?.display_address && (
                    <p>{locData.location.display_address.join(', ')}</p>
                )}
                {locData.hours && (
                    <div>
                        <p> Hours:</p>
                        {locData.hours.map((day, index) => (
                            <div key={index}>
                                {day.open.map((time, i) => (
                                    <span key={i}>
                                        {' '}
                                        <li>
                                            {time.start} - {time.end}
                                        </li>
                                    </span>
                                ))}
                            </div>
                        ))}
                    </div>
                )}
            </div>
        </div>
    )
}
