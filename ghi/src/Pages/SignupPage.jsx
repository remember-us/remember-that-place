import { useState } from 'react'
import { useAuthContext } from '@galvanize-inc/jwtdown-for-react'
import { useNavigate } from 'react-router-dom'
import { login, register } from '../services/auth'

const SignupPage = () => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [city, setCity] = useState('')
    const { baseUrl, setToken } = useAuthContext()
    const [errorMessage, setErrorMessage] = useState('')
    const navigate = useNavigate()

    const handleSignup = async (e) => {
        e.preventDefault()
        const form = e.currentTarget
        const accountData = {
            username: username,
            password: password,
            city: city,
        }
        try {
            await register(accountData)
            const token = await login(
                baseUrl,
                accountData.username,
                accountData.password
            )
            setToken(token)
            form.reset()
            navigate('/')
        } catch (e) {
            if (e instanceof Error) {
                setErrorMessage(e.message)
            }
            console.error(e)
        }
    }

    const handleUsernameChange = (e) => {
        const value = e.target.value
        setUsername(value)
    }

    const handlePasswordChange = (e) => {
        const value = e.target.value
        setPassword(value)
    }

    const handleCityChange = (e) => {
        const value = e.target.value
        setCity(value)
    }

    return (
        <>
            <div className="my-5 text-center p-5">
                <h1 className="display-5 fw-bold bg-danger-subtle w-200px rounded-pill">
                    Sign Up
                </h1>
            </div>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4 border border-danger-subtle">
                        <form onSubmit={handleSignup}>
                            <div className="form-floating mb-3">
                                {errorMessage ? <p>{errorMessage}</p> : ''}
                                <input
                                    onChange={handleUsernameChange}
                                    placeholder="Username"
                                    required
                                    type="text"
                                    name="username"
                                    id="username"
                                    className="form-control"
                                />
                                <label htmlFor="username">Username</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handlePasswordChange}
                                    placeholder="Password"
                                    required
                                    type="password"
                                    name="password"
                                    id="password"
                                    className="form-control"
                                />
                                <label htmlFor="password">Password</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleCityChange}
                                    placeholder="City"
                                    required
                                    type="text"
                                    name="city"
                                    id="city"
                                    className="form-control"
                                />
                                <label htmlFor="city">City</label>
                            </div>
                            <div>
                                <center>
                                    <input
                                        className="btn btn-danger"
                                        type="submit"
                                        value="Signup"
                                    />
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default SignupPage
