import { useState, useEffect } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import useToken from '@galvanize-inc/jwtdown-for-react'

const API_HOST = import.meta.env.VITE_API_HOST

function SearchResults() {
    const [searchResults, setSearchResults] = useState([])
    const location = useLocation()
    const { token } = useToken()
    const navigate = useNavigate()

    const fetchData = async (query) => {
        try {
            const response = await fetch(
                `${API_HOST}/locations/search?location=${query}&term=${query}`
            )
            if (response.ok) {
                const data = await response.json()
                setSearchResults(data.Response.businesses)
            } else {
                console.error('Failed to fetch search results')
            }
        } catch (error) {
            console.error('Error fetching search results:', error)
        }
    }

    const handleSubmit = async (name, image_url, id) => {
        if (!token) {
            console.log('Please log in to add favorites')
            navigate('/login')
        }

        const favsUrl = `${API_HOST}/favorites/`
        const favData = {
            name: name,
            image_url: image_url,
            reference_id: id,
        }

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(favData),
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: 'include',
        }

        try {
            const response = await fetch(favsUrl, fetchConfig)
            if (!response.ok) {
                alert('Couldnt like that location, sorry!')
            } else {
                alert('Liked location!')
            }
        } catch (error) {
            console.error('Error liking location:', error)
        }
    }

    useEffect(() => {
        const searchParams = new URLSearchParams(location.search)
        const query = searchParams.get('query')
        if (query) {
            fetchData(query)
        }
    }, [location.search, token])

    return (
        <div>
            <div className="p-3">
                {searchResults.map((result) => (
                    <div key={result.id} className="p-3 d-md-inline-grid">
                        <div
                            className="card border border-danger-subtle border-3"
                            style={{ width: '18rem' }}
                        >
                            <a href={`/${result.id}`}>
                                <img
                                    className="card-img-top"
                                    src={result.image_url}
                                    alt="Result image"
                                />
                            </a>
                            <p>
                                {' '}
                                {result.name}
                                <br />
                                {result.rating}
                                <br />
                                Phone Number:{result.display_phone}
                                <br />
                                Open Now: {result.is_closed ? 'No' : 'Yes'}
                            </p>
                            <button
                                className="btn btn-danger"
                                type="button"
                                onClick={() =>
                                    handleSubmit(
                                        result.name,
                                        result.image_url,
                                        result.alias
                                    )
                                }
                            >
                                Like!
                            </button>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default SearchResults
