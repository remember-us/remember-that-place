import useToken from '@galvanize-inc/jwtdown-for-react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'

const LoginForm = () => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const { login } = useToken()
    const navigate = useNavigate()

    const handleSubmit = (e) => {
        e.preventDefault()
        login(username, password)
        e.target.reset()
        navigate('/')
    }

    return (
        <div className="card text-bg-light mb-3 p-5">
            <div className="my-5 text-center">
                <h1 className="display-5 fw-bold bg-danger-subtle w-200px rounded-pill">
                    Log in
                </h1>
            </div>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4 border border-danger-subtle">
                        <form onSubmit={(e) => handleSubmit(e)}>
                            <div className="mb-3">
                                <label className="form-floating mb-3">
                                    Username:
                                </label>
                                <input
                                    name="username"
                                    type="text"
                                    className="form-control"
                                    onChange={(e) =>
                                        setUsername(e.target.value)
                                    }
                                />
                            </div>
                            <div className="mb-3">
                                <label className="form-floating mb-3">
                                    Password:
                                </label>
                                <input
                                    name="password"
                                    type="password"
                                    className="form-control"
                                    onChange={(e) =>
                                        setPassword(e.target.value)
                                    }
                                />
                            </div>
                            <div>
                                <input
                                    className="btn btn-danger"
                                    type="submit"
                                    value="Login"
                                />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoginForm
