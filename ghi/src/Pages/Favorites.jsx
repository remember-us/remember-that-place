import { useState, useEffect } from 'react'
import PageHeader from '../components/Header'
import NoteCard from '../components/NoteCard'

const API_HOST = import.meta.env.VITE_API_HOST

function FavoritesList() {
    const [favorites, setFavorites] = useState([])
    const [body, setBody] = useState('')

    const fetchData = async () => {
        const favUrl = `${API_HOST}/favorites/`
        const request = await fetch(favUrl, { credentials: 'include' })
        if (request.ok) {
            const response = await request.json()
            setFavorites(response)
        } else {
            console.log('data fetch error')
        }
    }

    const changeBody = async (event) => {
        setBody(event.target.value)
    }

    const handleCreateNote = async (favorite_id) => {
        const noteUrl = `${API_HOST}/favorites/${favorite_id}/notes/`
        const noteData = { body: body }

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(noteData),
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: 'include',
        }

        try {
            const response = await fetch(noteUrl, fetchConfig)
            if (response.ok) {
                fetchData()
            } else {
                console.error('Failed to create note:', response.statusText)
            }
        } catch (error) {
            console.error('Error creating note:', error)
        }
    }

    const handleDeleteFavorite = async (favorite_id) => {
        try {
            const response = await fetch(
                `${API_HOST}/favorites/${favorite_id}`,
                { method: 'DELETE', credentials: 'include' }
            )
            if (response.ok) {
                fetchData()
            } else {
                console.error('Failed to delete:', response.statusText)
            }
        } catch (error) {
            console.error('Error deleting note:', error)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div>
            <PageHeader />
            <div className="p-3 ">
                {favorites.map((favorite) => (
                    <div
                        key={favorite.favorite_id}
                        className="p-3 d-md-inline-grid"
                    >
                        <div
                            className="card border border-danger-subtle border-3"
                            style={{ width: '18rem' }}
                        >
                            <a href={`/${favorite.reference_id}`}>
                                <img
                                    className="card-img-top"
                                    src={favorite.image_url}
                                    alt="fav image"
                                />
                            </a>
                            <div className="card-body">
                                <h5 className="card-title">{favorite.name}</h5>
                                <NoteCard favorite_id={favorite.favorite_id} />
                                <input onChange={changeBody}></input>
                                <button
                                    className="btn btn-primary"
                                    onClick={() =>
                                        handleCreateNote(favorite.favorite_id)
                                    }
                                >
                                    Add Note
                                </button>
                                <button
                                    className="btn btn-danger"
                                    onClick={() =>
                                        handleDeleteFavorite(
                                            favorite.favorite_id
                                        )
                                    }
                                >
                                    Delete Favorite
                                </button>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default FavoritesList
