import useToken from '@galvanize-inc/jwtdown-for-react'
import { useState } from 'react'
import { NavLink, useNavigate } from 'react-router-dom'

function Navbar() {
    const { baseUrl, token, logout } = useToken()

    const navigate = useNavigate()
    const [query, setQuery] = useState('')

    const handleChange = (event) => {
        setQuery(event.target.value)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        navigate(`/search?query=${query}`)
    }

    return (
        <nav
            className="navbar navbar-dark navbar-expand-lg bg-gradient sticky-top"
            style={{ backgroundColor: '#663344' }}
        >
            <div className="container-fluid">
                <NavLink className="navbar-brand" id="RTPNavTitle" to="/">
                    Remember That Place
                </NavLink>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div
                    className="collapse container-fluid navbar-collapse"
                    id="navbarSupportedContent"
                >
                    <ul className="navbar-nav">
                        <li>
                            <form className="d-flex" onSubmit={handleSubmit}>
                                <input
                                    className="form-control input-SearchBar"
                                    type="search"
                                    placeholder="Search Location"
                                    aria-label="Search"
                                    id="navBarSearchForm"
                                    value={query}
                                    onChange={handleChange}
                                />
                            </form>
                        </li>
                        {!token && (
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/login">
                                    Log In
                                </NavLink>
                            </li>
                        )}
                        {!token && (
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/signup">
                                    Sign Up
                                </NavLink>
                            </li>
                        )}
                        {token && (
                            <>
                                <li className="nav-item">
                                    <NavLink
                                        className="nav-link"
                                        to="/"
                                        onClick={() => logout(baseUrl, token)}
                                    >
                                        Log Out
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        className="nav-link"
                                        to="/favorites"
                                    >
                                        My Favorites
                                    </NavLink>
                                </li>
                            </>
                        )}
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Navbar
