import { useState, useEffect } from 'react'
import useToken from '@galvanize-inc/jwtdown-for-react'

const API_HOST = import.meta.env.VITE_API_HOST

function LocationCarousel(props) {
    const [locations, setLocations] = useState([])
    const { token } = useToken()

    const fetchData = async () => {
        let locationUrl = `${API_HOST}/locations/search?location=${props.location}&term=${props.term}&sort_by=best_match&limit=20`
        const request = await fetch(locationUrl)
        if (request.ok) {
            const response = await request.json()
            setLocations(response.Response.businesses)
        } else {
            console.log('error')
        }
    }

    const handleSubmit = async (name, image_url, id) => {
        const favsUrl = `${API_HOST}/favorites/`
        const favData = {
            name: name,
            image_url: image_url,
            reference_id: id,
        }

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(favData),
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: 'include',
        }
        const response = await fetch(favsUrl, fetchConfig)
        if (!response.ok) {
            alert('Couldnt like that location, sorry!')
        } else {
            alert('liked location!')
        }
    }

    useEffect(() => {
        fetchData()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <div className="px-4 py-5 my-5 text-center">
            <div className="col-lg-6 mx-auto">
                <h1 className="display-5 fw-bold bg-danger-subtle w-200px rounded-pill">
                    {props.title}
                </h1>
                <div
                    id={`LocCarousel${props.c}`}
                    className="carousel slide container"
                >
                    <div className="carousel-inner bg-white rounded-3">
                        {locations?.map((location, index) => {
                            return (
                                <div
                                    data-bs-target={`#LocCarousel${props.c}`}
                                    key={location.id}
                                    className={`${
                                        index === 0
                                            ? 'carousel-item active border border-4 bg-opacity-25 rounded-3 p-2'
                                            : 'carousel-item border border-4 bg-opacity-25 rounded-3 p-2'
                                    }`}
                                >
                                    <a href={`/${location.id}`}>
                                        <img
                                            src={location.image_url}
                                            className="d-block w-100 rounded-2"
                                            style={{
                                                objectFit: 'cover',
                                                objectPosition: '50% 60%',
                                                height: '350px',
                                                width: '350px',
                                            }}
                                            alt="None Found"
                                            key={location.alias}
                                        ></img>
                                    </a>
                                    <p>
                                        {location.name}
                                        <br />
                                        {location.rating}
                                        <br />
                                        Phone Number: {location.display_phone}
                                        <br />
                                        Open Now:{' '}
                                        {location.is_closed ? 'No' : 'Yes'}
                                    </p>
                                    {token && (
                                        <button
                                            className="btn btn-danger"
                                            type="submit"
                                            onClick={() =>
                                                handleSubmit(
                                                    location.name,
                                                    location.image_url,
                                                    location.alias
                                                )
                                            }
                                        >
                                            Like!
                                        </button>
                                    )}
                                </div>
                            )
                        })}
                    </div>
                    <button
                        className="carousel-control-prev"
                        type="button"
                        data-bs-target={`#LocCarousel${props.c}`}
                        data-bs-slide="prev"
                    >
                        <span
                            className="carousel-control-prev-icon"
                            aria-hidden="true"
                        ></span>
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button
                        className="carousel-control-next"
                        type="button"
                        data-bs-target={`#LocCarousel${props.c}`}
                        data-bs-slide="next"
                    >
                        <span
                            className="carousel-control-next-icon"
                            aria-hidden="true"
                        ></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </div>
    )
}

export default LocationCarousel
