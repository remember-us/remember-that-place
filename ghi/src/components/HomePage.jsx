import LocationCarousel from './Carousel'
import PageHeader from './Header'
import { useEffect, useState } from 'react'

const API_HOST = import.meta.env.VITE_API_HOST

function HomePage() {
    const [userData, setUserData] = useState({})

    const GrabToken = async () => {
        const url = `${API_HOST}/token`
        const request = await fetch(url, { credentials: 'include' })
        const body = await request.json()
        setUserData(body)
    }

    useEffect(() => {
        GrabToken()
    }, [])

    return (
        <div>
            <PageHeader />
            <LocationCarousel
                title="Food!"
                location="nyc"
                term="food"
                c="foo"
            />
            {userData?.account && (
                <LocationCarousel
                    title="Stores Near you!"
                    location={userData.account.city}
                    term="shop"
                    c="bar"
                />
            )}
        </div>
    )
}

export default HomePage
