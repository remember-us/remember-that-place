import { useState, useEffect } from 'react'

const API_HOST = import.meta.env.VITE_API_HOST

function NoteCard({ favorite_id }) {
    const [notes, setNotes] = useState([])

    const fetchData = async () => {
        const noteUrl = `${API_HOST}/favorites/${favorite_id}/notes/`
        try {
            const response = await fetch(noteUrl, { credentials: 'include' })
            if (response.ok) {
                const data = await response.json()
                setNotes(data)
            } else {
                console.error('Failed to fetch notes')
            }
        } catch (error) {
            console.error('Error fetching notes:', error)
        }
    }
    useEffect(() => {
        fetchData()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [favorite_id])

    const handleDelete = async (note_id) => {
        try {
            const response = await fetch(
                `${API_HOST}/favorites/${favorite_id}/notes/${note_id}`,
                { method: 'DELETE' }
            )
            if (response.ok) {
                fetchData()
            } else {
                console.error('Failed to delete:', response.statusText)
            }
        } catch (error) {
            console.error('Error deleting note:', error)
        }
    }
    return (
        <div>
            <p>NOTES</p>
            {notes.map((note) => (
                <div key={note.note_id}>
                    <div className="card" style={{ width: '18rem' }}>
                        <div className="card-body">
                            <h5 className="card-title">{note.name}</h5>
                            <p className="card-text">{note.body}</p>
                            <button
                                className="btn btn-danger"
                                onClick={() => handleDelete(note.note_id)}
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    )
}
export default NoteCard
