import './app/App.css'
import Navbar from './components/Nav'
import HomePage from './Pages/HomePage'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { AuthProvider } from '@galvanize-inc/jwtdown-for-react'
import SignupPage from './Pages/SignupPage'
import SearchPage from './Pages/SearchPage'
import LoginForm from './Pages/LoginForm'
import FavoritesList from './Pages/Favorites'
import LocationDetail from './Pages/LocationDetail'

const API_HOST = import.meta.env.VITE_API_HOST

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}

function App() {

    const domain = /https:\/\/[^/]+/
    const basename = import.meta.env.VITE_PUBLIC_URL.replace(domain, '')
    console.log(import.meta.env)

    return (
        <AuthProvider baseUrl={API_HOST}>
            <BrowserRouter basename={basename}>
                <Navbar />
                <Routes>
                    <Route index element={<HomePage />} />
                    <Route path="login" element={<LoginForm />} />
                    <Route path="signup" element={<SignupPage />} />
                    <Route path="search" element={<SearchPage />} />
                    <Route path="favorites" element={<FavoritesList />} />
                    <Route path=":id" element={<LocationDetail />} />
                </Routes>
            </BrowserRouter>
        </AuthProvider>
    )
}

export default App
