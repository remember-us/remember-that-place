# Remember That Place

Remember That Place - Your very own personalized database of your favorite restaurants and stores nearby you.

Can't recall that diner that serves epic pancakes that you went to in the past year?

Need to make a note of when that 50% off clothes sale ends where you won't forget?

No worries - Remember That Place by name, Remember That Place by nature.

<details>
<summary>Table of Contents</summary>

[TOC]

</details>

## Authors

-   [Blake Frederick](@Blakefred)
-   [Sean O'Hara](@seanoharadev)
-   [Kyu Sub Shin](@KyusGitCodin94)
-   [Xetta Snyder](@Xetta.Snyder)

## Onboarding

**Ensure you have [Docker][docker link], [Git][git link], and [Node.js][node link] 20.10.0 or later, and you will also need to have a _Yelp API Key_. You can find out more and request one [here](https://www.yelp.com/login?return_url=/developers/v3/manage_app)**

1. Fork this repository.

2. Clone the forked repository into a local directory with the command:

```
git clone <<repository.url.here>>
```

3. Open the project directory.

4. Make a copy of the `.env.sample` file and rename it to `.env` in both the main project directory and the `ghi` directory.

5. You will need to update the items in the `.env` file to what you want your local development environment to be.

    - pgsqlPW
        - The password for your local postgres database.
    - pgsqlUser
        - The username for your local postgres database.
    - pgsqlDB
        - The name of the local postgres database.
    - pgadminEmail
        - The email address for the local PG-Admin account.
    - pgadminPW
        - The PG-Admin account password.
    - pgsqlDBUrl
        - The URL for your local postgres database.
    - SIGNING_KEY
        - Part of the JWT signing key.
    - YELP_API_KEY
        - Your Yelp API Key to have access to that functionality.

<br>

6. Build and run the project using Docker from the project top-level directory with the following commands:

```
docker volume create postgres-data
docker volume create pg-admin
docker-compose build
docker-compose up
```

7. Verify all of your docker containers are running in the desktop application.

8. View the project locally in the browser at:
   http://localhost:5173/

## Intended Market

Our application is intended for outdoors lovers looking to keep a convenient, personalized collection of the best restaurants and stores in any city, along with personal memos attached to these locations.

## User Stories/Scenarios

### Feature 1: Favorited locations

Logged-in users can control what locations they want to show up in their favorites page

#### Scenario 1: Favoriting a location

-   Given a user is logged in
-   When the user "likes" a location
-   Then it appears on the user's favorites page

#### Scenario 2: Un-favoriting a location

-   Given a user is logged in and in the favorites page
-   When the user deletes a location
-   Then it is removed from the user's favorites page

### Feature 2: Notes

Logged-in users can add a note to and delete a note from a favorited location

#### Scenario 1: Adding a note

-   Given a user is logged in and in the favorites page
-   When the user adds a note for a particular location
-   Then it appears on that location in the user's favorites page

#### Scenario 2: Deleting a note

-   Given a user is logged in and in the favorites page
-   When the user deletes a note for a particular location
-   Then it is removed from that location in the user's favorites page

## Functionality

-   General
    -   Sign up for a new account
    -   Account login/logout
    -   Main page with revolving carousels displaying recommended restaurants and stores
-   After login
    -   Main page displaying recommended restaurants and stores near the user's location
        -   Like button to add restaurants/stores to a "favorites" list
    -   Favorites page displaying the restaurants/stores the user "liked"
        -   Users can view, add, and delete notes for each favorited location
        -   Users can also un-favorite favorited locations
    -   Search bar to look up locations

## Tech Stack

The following languages/frameworks/technologies were used for this project:

-   Python
-   FastAPI
-   PostgreSQL
-   JavaScript
-   React
-   Bootstrap

## Documentation

-   [API documentation](docs/api_documentation.md)
-   [DB schemas](docs/db_schemas.md)
-   [Wireframe](docs/wireframe.png)

## Testing

-   Kyu Sub Shin [Getting a specific note](api/tests/test_get_note.py)

    -   This unit test tests the retrieval of a specific note that the user created for a specific location.

        We first define a mock notes query with mock get_note() function that returns mock note output data, as well as a fake_current_account_data function that returns mock input account data for the router function call for getting a specific note (get_one_note()).

        The actual unit test starts by arranging the arguments for the get_one_note() router call. The test uses dependency injection to override the account data and notes query parameters with the mock data and function defined above. It also defines mock favorite and note ids that will be passed into the call.

        The test then makes the router call using the arguments provided above. It then verifies that the response is successful with a 200 OK and that the content of the returned note is the same as the arguments we passed in earlier.

-   Xetta Snyder [Creating a favorite](api/tests/test_make_favorite.py)

    -   This test has a mock database dependency and a mock account dependency so dont worry you wont actually need a database or account for it to work
    -   The test simply uses preset data to test the **POST** `/favorites` route. The unit test must be run in a docker container using the `python` command and `pytest` as a module of it. This would look like `python -m pytest`.
    -   If this test fails, the favorites post route is broken. Please contact one of our team members for assistance.

-   Sean O'Hara [Creating a note](api/tests/test_make_note.py)
    -   This test uses mock data to test the **POST** `/favorites/{favorite_id}/notes` route. The unit test must be run in a docker container using the `python -m pytest`
    -   If this test fails, the notes post route is broken. Please contact one of our team members for assistance.

Blake Frederick [Getting a specific favorite](api/tests/test_get_favorite.py)

    -   This unit test tests the retrieval of a specific favorite that the user created for a specific location. It is designed to test out the functionality of the **GET** route `/favorites`/
         We first define a mock favorites query with the test_get_favorite() function that returns mock favorite output data, as well as a fake_favorites_data function that returns mock input favorites data for the router function call for getting a specific favorate (get_favorite()).\
         The actual unit test starts by arranging the arguments for the get_favorite() router call. The test uses dependency injection to override the favorites data and favorites query parameters with the mock data and function defined above. It also defines mock a account id that will be passed into the call.\
         The test then makes the router call using the arguments provided above. It then verifies that the response is successful with a 200 OK and that the content of the returned favorite is the same as the arguments we passed in earlier. This can be tested out by using pytest in the fastapi docker container.

[docker link]: https://www.docker.com/
[node link]: https://nodejs.org/
[git link]: https://git-scm.com/
