import os
from datetime import timedelta
from typing import Optional
from fastapi import Depends, Cookie, HTTPException, status, Response, Request
from jwtdown_fastapi.authentication import Authenticator
from pydantic import BaseModel
from queries.accounts import AccountOut, AccountOutWithPassword, AccountQueries
from jose.constants import ALGORITHMS
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from passlib.context import CryptContext
from jose import ExpiredSignatureError, JWTError, jwt


class Token(BaseModel):
    """Represents a bearer token."""

    access_token: str
    """Contains the encoded JWT"""

    token_type: str = "Bearer"
    """This is set to "Bearer" because it's a bearer token."""

    account_id: int


class BadAccountDataError(RuntimeError):
    """Occurs when account data cannot be converted to a
    dictionary.


    Raised when trying to convert account data into a
    subject claim and a dictionary of account data.
    """

    pass


class RTPAuthenticator(Authenticator):
    def __init__(
        self,
        key: str,
        /,
        algorithm: str = ALGORITHMS.HS256,
        cookie_name: str = "fastapi_token",
        path: str = "token",
        exp: timedelta = timedelta(hours=1),
        public_key=None,
    ):
        self.cookie_name = cookie_name or self.COOKIE_NAME
        self.key = key
        self.algorithm = algorithm
        self.path = path
        self.scheme = OAuth2PasswordBearer(tokenUrl=path, auto_error=False)
        self._router = None
        self.pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
        self.exp = exp
        self.public_key = public_key

        async def _try_jwt(
            self,
            bearer_token: Optional[str] = Depends(self.scheme),
            cookie_token: Optional[str] = (
                Cookie(default=None, alias=self.cookie_name)
            ),
            session_getter=Depends(self.get_session_getter),
        ):
            token = bearer_token
            if not token and cookie_token:
                token = cookie_token
            try:
                if public_key:
                    decode_key = public_key
                else:
                    decode_key = key
                payload = jwt.decode(token, decode_key, algorithms=[algorithm])
                if "jti" in payload:
                    jti = payload["jti"]
                    is_valid = await self.validate_jti(jti, session_getter)
                    if is_valid:
                        return payload
                    else:
                        await self.jti_destroyed(jti, session_getter)
            except ExpiredSignatureError:
                claims = jwt.get_unverified_claims(token)
                if "jti" in claims:
                    await self.jti_destroyed(claims["jti"], session_getter)
            except (JWTError, AttributeError):
                pass
            return None

        setattr(
            self,
            "_try_jwt",
            _try_jwt.__get__(self, self.__class__),
        )

        async def try_account_data(self, token: dict = Depends(self._try_jwt)):
            if token and "account" in token:
                return token["account"]
            return None

        setattr(
            self,
            "try_get_current_account_data",
            try_account_data.__get__(self, self.__class__),
        )

        async def account_data(
            self,
            data: dict = Depends(self.try_get_current_account_data),
        ) -> Optional[dict]:
            if data is None:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail="Invalid token",
                    headers={"WWW-Authenticate": "Bearer"},
                )
            return data

        setattr(
            self,
            "get_current_account_data",
            account_data.__get__(self, self.__class__),
        )

        async def login(
            self,
            response: Response,
            request: Request,
            form: OAuth2PasswordRequestForm = Depends(),
            account_getter=Depends(self.get_account_getter),
            session_getter=Depends(self.get_session_getter),
        ) -> Token:
            return await Authenticator.login(
                self,
                response,
                request,
                form,
                account_getter,
                session_getter,
            )

        setattr(
            self,
            "login",
            login.__get__(self, self.__class__),
        )

        async def logout(
            self,
            request: Request,
            response: Response,
            session_getter=Depends(self.get_session_getter),
            jwt: dict = Depends(self._try_jwt),
        ) -> Token:
            return await Authenticator.logout(
                self,
                request,
                response,
                session_getter,
                jwt,
            )

        setattr(
            self,
            "logout",
            logout.__get__(self, self.__class__),
        )

    async def get_account_data(
        self,
        username: str,
        accounts: AccountQueries,
    ):
        return accounts.get(username)

    def get_account_getter(
        self,
        accounts: AccountQueries = Depends(),
    ):
        return accounts

    def get_hashed_password(self, account: AccountOutWithPassword):
        return account.hashed_password

    def get_account_data_for_cookie(self, account: AccountOut):
        return account.username, AccountOut(**account.dict())


authenticator = RTPAuthenticator(
    os.environ["SIGNING_KEY"],
)
