from pydantic import BaseModel
from queries.pool import pool
from typing import Optional, Union, List


class Error(BaseModel):
    message: str


class NoteIn(BaseModel):
    body: str
    favorite_id: int
    account_id: int


class NoteOut(BaseModel):
    note_id: int
    body: str
    favorite_id: int
    account_id: int


class UpdateNoteIn(BaseModel):
    body: str
    account_id: int | None
    favorite_id: int | None


class NotesQueries(BaseModel):
    def get_all(self, favorite_id, account_id) -> Union[Error, List[NoteOut]]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT note_id, body, favorite_id, account_id
                    FROM notes
                    WHERE favorite_id = %s AND account_id = %s
                    ORDER BY note_id;
                    """,
                    [favorite_id, account_id],
                )

                return [self.record_to_note_out(record) for record in cur]

    def get_note(
        self, note_id: int, account_id: int, favorite_id: int
    ) -> Optional[NoteOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT note_id, account_id, favorite_id, body
                    FROM notes
                    WHERE note_id = %s AND account_id = %s AND favorite_id = %s
                    """,
                    [note_id, account_id, favorite_id],
                )
                try:
                    record = None
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    if record is not None:
                        return NoteOut(**record)
                except Exception as e:
                    print("exception:", str(e))
                    raise

    def create_note(self, note: NoteIn) -> NoteOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                try:
                    result = cur.execute(
                        """
                        INSERT INTO notes (
                        body, favorite_id, account_id
                        )
                        VALUES (%s, %s, %s)
                        RETURNING note_id;
                        """,
                        [note.body, note.favorite_id, note.account_id],
                    )

                    note_id = result.fetchone()[0]
                    return self.NoteInOut(note_id, note)

                except Exception as e:
                    print("Exception:", str(e))
                    raise

    def update_note(self, note_id: int, note: UpdateNoteIn) -> NoteOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    UPDATE notes
                    SET body = %s
                    WHERE note_id = %s
                    """,
                    [note.body, note_id],
                )
                return self.NoteInOut(note_id, note)

    def delete_note(self, note_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM notes
                    WHERE note_id = %s
                    """,
                    [note_id],
                )
                return True

    def NoteInOut(self, note_id: int, note: NoteIn) -> NoteOut:
        old_data = note.dict()
        return NoteOut(note_id=note_id, **old_data)

    def record_to_note_out(self, record) -> NoteOut:
        return NoteOut(
            note_id=record[0],
            body=record[1],
            favorite_id=record[2],
            account_id=record[3],
        )
