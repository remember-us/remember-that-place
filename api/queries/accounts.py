from fastapi import HTTPException
from pydantic import BaseModel
from queries.pool import pool


class DuplicateAccountError(ValueError):
    message: str


class AccountIn(BaseModel):
    username: str
    password: str
    city: str


class AccountOut(BaseModel):
    account_id: int
    username: str
    city: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountToken(BaseModel):
    access_token: str
    type: str
    account: AccountOut


class AccountQueries(BaseModel):
    def get(self, username: str) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT account_id, username, city, hashed_password
                    FROM accounts
                    WHERE username = %s
                    """,
                    [username],
                )
                try:
                    record = None
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    if record is not None:
                        return AccountOutWithPassword(**record)
                except Exception as e:
                    print("exception:", str(e))
                    raise

    def create(self, data, hashed_password) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                try:
                    params = [
                        data.username,
                        data.city,
                        hashed_password,
                    ]
                    cur.execute(
                        """
                        INSERT INTO accounts (
                        username,
                        city,
                        hashed_password
                        )
                        VALUES (%s, %s, %s)
                        RETURNING
                        account_id, username, city, hashed_password
                        """,
                        params,
                    )

                    record = None
                    row = cur.fetchone()
                    if row is not None:
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]

                    return AccountOutWithPassword(**record)

                except DuplicateAccountError:
                    print(
                        "Duplicate account error:",
                        "Cannot create an account with those credentials",
                    )

                except Exception as e:
                    print("Exception:", str(e))
                    raise

    def get_by_username(self, username: str) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT account_id, username, city, hashed_password
                    FROM accounts
                    WHERE username = %s
                    """,
                    [username],
                )
                try:
                    record = None
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    if record is not None:
                        return AccountOutWithPassword(**record)
                except Exception as e:
                    print("exception:", str(e))
                    raise

    def update(
        self, account_id: int, data: AccountIn, hashed_password
    ) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    UPDATE accounts
                    SET username = %s, city = %s, hashed_password = %s
                    WHERE account_id = %s
                    RETURNING account_id, username, city, hashed_password
                    """,
                    [data.username, data.city, hashed_password, account_id],
                )
                updated_account = cur.fetchone()
                if updated_account is not None:
                    account_dict = {
                        "account_id": updated_account[0],
                        "username": updated_account[1],
                        "city": updated_account[2],
                        "hashed_password": updated_account[3],
                    }
                    return AccountOutWithPassword(**account_dict)
                else:
                    raise HTTPException(
                        status_code=404, detail="Account not found"
                    )
