from pydantic import BaseModel
from queries.pool import pool


class FavoriteIn(BaseModel):
    """_summary_

    Args:
        BaseModel (_type_): _description_
    """

    name: str
    image_url: str
    reference_id: str


class FavoriteOut(BaseModel):
    favorite_id: int
    name: str
    image_url: str
    account_id: int
    reference_id: str


class FavoritesQueries(BaseModel):
    def create_favorite(self, favorite, account_id) -> FavoriteOut:
        print("Favorite:", favorite)
        with pool.connection() as conn:
            with conn.cursor() as cur:
                try:
                    params = [
                        favorite.name,
                        favorite.image_url,
                        account_id,
                        favorite.reference_id,
                    ]
                    cur.execute(
                        """
                        INSERT INTO favorites (
                        name, image_url, account_id, reference_id
                        )
                        VALUES (%s, %s, %s, %s)
                        RETURNING favorite_id, name, image_url,
                        account_id, reference_id;
                        """,
                        params,
                    )

                    record = None
                    row = cur.fetchone()
                    if row is not None:
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    return FavoriteOut(**record)

                except Exception as e:
                    print("Exception:", str(e))
                    raise

    def get_favorite(
        self, favorite_id: int, account_id: int
    ) -> FavoriteOut | None:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT
                    favorite_id, name, image_url, reference_id, account_id
                    FROM favorites
                    WHERE favorite_id = %s AND account_id = %s
                    """,
                    [favorite_id, account_id],
                )
                try:
                    record = None
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    if record is not None:
                        return FavoriteOut(**record)
                except Exception as e:
                    print("exception from api:", str(e))
                    raise

    def get_favorites(self, account_id: int) -> dict | None:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT * FROM favorites
                    WHERE account_id = %s
                    """,
                    [account_id],
                )
                try:
                    map = result.fetchall()
                    return [
                        self.record_to_favorite_out(record) for record in map
                    ]

                except Exception as e:
                    print("exception from api:", str(e))
                    raise

    def record_to_favorite_out(self, record) -> FavoriteOut:
        return FavoriteOut(
            favorite_id=record[0],
            name=record[1],
            image_url=record[2],
            reference_id=record[3],
            account_id=record[4],
        )

    def delete_favorite(self, favorite_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM favorites
                    WHERE favorite_id = %s
                    """,
                    [favorite_id],
                )
                return True
