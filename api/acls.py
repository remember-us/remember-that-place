import os
import json
import requests
import random

yelp_Key = os.environ["YELP_API_KEY"]


def GetLocations(location: str, term: str | None):
    url = "https://api.yelp.com/v3/businesses/search"
    headers = {
        "accept": "application/json",
        "Authorization": f"Bearer {yelp_Key}",
    }
    parameters = {"location": location, "limit": 20, "sort_by": "best_match"}
    if term is not None:
        parameters["term"] = term
    response = requests.get(url, headers=headers, params=parameters)
    content = json.loads(response.content)
    try:
        random.shuffle(content["businesses"])
        return {"Response": content}
    except (KeyError, IndexError):
        return {"Response": "error"}


def GetLocationDetail(query):
    url = f"https://api.yelp.com/v3/businesses/{query}"
    headers = {
        "accept": "application/json",
        "Authorization": f"Bearer {yelp_Key}",
    }
    response = requests.get(url, headers=headers)
    content = json.loads(response.content)
    try:
        return {"Response": content}
    except (KeyError, IndexError):
        return {"Response": "error"}
