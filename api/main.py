import os

# from psycopg_pool import ConnectionPool
from authenticator import authenticator
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import Accounts, Favorites, Locations, notes

tags_metadata = [
    {
        "name": "Accounts",
        "description": "_Search description here_",
    },
    {
        "name": "Auth",
        "description": "Operations with users. The **login** \
            logic is also here.",
    },
    {
        "name": "Favorites",
        "description": "_Favorites description here_",
    },
    {
        "name": "Notes",
        "description": "_Notes description here_",
    },
    {
        "name": "Locations",
        "description": "_Locations description here_",
    },
]

app = FastAPI(openapi_tags=tags_metadata)
app.include_router(Accounts.router, tags=["Accounts"])
app.include_router(authenticator.router, tags=["Auth"])
app.include_router(Locations.router, tags=["Locations"])
app.include_router(Favorites.router, tags=["Favorites"])
app.include_router(notes.router, tags=["Notes"])

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }
