from typing import Union

from fastapi import APIRouter, Depends, Response
from pydantic import BaseModel
from queries.favorites import FavoriteIn, FavoriteOut, FavoritesQueries
from authenticator import Token, authenticator


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/favorites")
async def get_favorites(
    response: Response,
    favs: FavoritesQueries = Depends(),
    token: Token = Depends(authenticator.get_current_account_data),
):
    get_fav = favs.get_favorites(account_id=token["account_id"])
    if get_fav is None:
        response.status_code = 404
    return get_fav


@router.get("/favorites/{favorite_id}")
async def get_favorite(
    favorite_id: int,
    response: Response,
    favs: FavoritesQueries = Depends(),
    token: Token = Depends(authenticator.get_current_account_data),
):
    get_fav = favs.get_favorite(
        favorite_id=favorite_id, account_id=token["account_id"]
    )
    if get_fav is None:
        response.status_code = 404
    return get_fav


@router.post("/favorites", response_model=Union[FavoriteOut, HttpError])
async def create_favorite(
    model: FavoriteIn,
    favorite: FavoritesQueries = Depends(),
    token: Token = Depends(authenticator.get_current_account_data),
) -> FavoriteOut:
    try:
        favorite = favorite.create_favorite(model, token["account_id"])
    except Exception as e:
        print("Exception:", str(e))
        raise

    return FavoriteOut(
        favorite_id=favorite.favorite_id,
        name=favorite.name,
        image_url=favorite.image_url,
        account_id=favorite.account_id,
        reference_id=favorite.reference_id,
    )


@router.delete("/favorites/{favorite_id}", response_model=bool)
def delete_favorite(
    favorite_id: int,
    repo: FavoritesQueries = Depends(),
    token: Token = Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.delete_favorite(favorite_id)
