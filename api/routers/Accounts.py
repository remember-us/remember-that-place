from authenticator import authenticator
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    Request,
    Response,
    status,
)
from jwtdown_fastapi.authentication import Token
from pydantic import BaseModel
from queries.accounts import (
    AccountIn,
    AccountOut,
    AccountQueries,
    DuplicateAccountError,
    AccountOutWithPassword,
)
from typing import Union


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: dict = Depends(authenticator.try_get_current_account_data)
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.post("/accounts/", response_model=Union[AccountToken, HttpError])
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    accounts: AccountQueries = Depends(),
) -> AccountToken:
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = accounts.create(info, hashed_password)
    except DuplicateAccountError:
        print(
            "Duplicate account error:",
            "Cannot create an account with those credentials",
        )
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    except Exception as e:
        print("Exception:", str(e))
        raise

    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, accounts)
    return AccountToken(account=account, **token.dict())


@router.put("/accounts/{account_id}", response_model=AccountOut)
async def update_account(
    data: AccountIn,
    accounts: AccountQueries = Depends(),
    token: Token = Depends(authenticator.get_current_account_data),
) -> AccountOutWithPassword:
    account_id = token["account_id"]
    hashed_password = authenticator.hash_password(data.password)
    updated_account = accounts.update(account_id, data, hashed_password)
    return updated_account


@router.get("/accounts/{username}", response_model=AccountOut)
async def get_account(
    username: str,
    response: Response,
    accounts: AccountQueries = Depends(),
    token: Token = Depends(authenticator.get_current_account_data),
) -> AccountOut:
    get_account = accounts.get(username)
    if get_account is None:
        response.status_code = 404
    return get_account
