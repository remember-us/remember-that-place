from fastapi import (
    APIRouter,
    Depends,
    Response,
)
from queries.notes import Error, NoteOut, NotesQueries, UpdateNoteIn
from typing import Union, Optional, List
from pydantic import BaseModel
from authenticator import Token, authenticator


router = APIRouter()


class CreateNoteIn(BaseModel):
    body: str
    account_id: int | None
    favorite_id: int | None


@router.get("/favorites/{favorite_id}/notes")
async def get_notes(
    favorite_id: int,
    response: Response,
    notes: NotesQueries = Depends(),
    token: Token = Depends(authenticator.get_current_account_data),
) -> Error | List[NoteOut]:
    account_id = token["account_id"]
    get_note = notes.get_all(favorite_id, account_id)
    if get_note is None:
        response.status_code = 404
    return get_note


@router.post(
    "/favorites/{favorite_id}/notes", response_model=Union[NoteOut, Error]
)
async def create_note(
    note: CreateNoteIn,
    favorite_id: int,
    response: Response,
    repo: NotesQueries = Depends(),
    token: Token = Depends(authenticator.get_current_account_data),
) -> NoteOut:
    note.favorite_id = favorite_id
    note.account_id = token["account_id"]
    if not note:
        response.status_code = 400
    return repo.create_note(note)


@router.get(
    "/favorites/{favorite_id}/notes/{note_id}",
    response_model=Optional[NoteOut],
)
def get_one_note(
    note_id: int,
    favorite_id: int,
    response: Response,
    repo: NotesQueries = Depends(),
    token: Token = Depends(authenticator.get_current_account_data),
) -> NoteOut:
    account_id = token["account_id"]
    note = repo.get_note(note_id, account_id, favorite_id)
    if note is None:
        response.status_code = 404
    return note


@router.put(
    "/favorites/{favorite_id}/notes/{note_id}",
    response_model=Union[NoteOut, Error],
)
def update_note(
    note_id: int,
    note: UpdateNoteIn,
    repo: NotesQueries = Depends(),
    token: Token = Depends(authenticator.get_current_account_data),
) -> Union[Error, NoteOut]:
    note.account_id = token["account_id"]
    return repo.update_note(note_id, note)


@router.delete("/favorites/{favorite_id}/notes/{note_id}", response_model=bool)
def delete_note(
    note_id: int,
    repo: NotesQueries = Depends(),
) -> bool:
    return repo.delete_note(note_id)
