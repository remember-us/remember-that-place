from acls import GetLocationDetail, GetLocations
from fastapi import APIRouter, Depends
from authenticator import Token, authenticator
from typing import Any
from fastapi import HTTPException


router = APIRouter()


@router.get("/locations/search")
async def search_locations(
    location: str, term: str | None = None
) -> dict[str, Any]:
    """
    Search for a location:

    - **location**: Must input a location entry.\
        Can be Zip code, City, State, Country, etc.
    - **term**: Optional term to search by. ex. "food", "coffee", "bars", etc.
    """
    try:
        response = GetLocations(location=location, term=term)
        if response["Response"] == "error":
            raise HTTPException(status_code=404, detail="Location not found")
        return response
    except Exception as e:
        response = {"Failed to get locations": e}


@router.get("/locations/{id_or_alias}")
async def grab_location(id_or_alias: str) -> dict:
    try:
        response = GetLocationDetail(id_or_alias)
    except Exception as e:
        response = {"Failed to get location": e}
    return response


@router.get("/locations")
async def locations_by_category(
    category: str,
    token: Token = Depends(authenticator.get_current_account_data),
) -> dict[str, Any]:
    zip = token["city"]
    try:
        response = GetLocations(location=zip, term=category)
    except Exception as e:
        response = {"Failed to get locations": e}
    return response
