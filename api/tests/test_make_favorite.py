from fastapi.testclient import TestClient
from queries.favorites import FavoritesQueries, FavoriteOut
from main import app
from authenticator import authenticator

client = TestClient(app)


class MockFavoritesQueries:
    def create_favorite(self, favorite, account_id) -> FavoriteOut:
        return FavoriteOut(
                favorite_id=1,
                name="Fake Name",
                image_url="fake.com",
                account_id=account_id,
                reference_id="fakeref",
            )


def fake_current_account_data():
    return {
        "account_id": 2,
        "username": "user",
        "city": 98765
    }


def test_create_favorite():
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_current_account_data
    app.dependency_overrides[FavoritesQueries] = MockFavoritesQueries
    fake_data = {
        "name": "Fake Name",
        "image_url": "fake.com",
        "reference_id": "fakeref",
        "account_id": 2
    }
    response = client.post("/favorites/", json=fake_data)
    assert response.status_code == 200
    assert response.json() == {
        "favorite_id": 1,
        "name": "Fake Name",
        "image_url": "fake.com",
        "account_id": 2,
        "reference_id": "fakeref",
    }

    app.dependency_overrides = {}
