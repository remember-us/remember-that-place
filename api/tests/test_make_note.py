from authenticator import authenticator
from fastapi.testclient import TestClient
from main import app
from queries.notes import NoteOut, NotesQueries

client = TestClient(app)


class MockNotesQueries:
    def create_note(self, note) -> NoteOut:
        return NoteOut(
                note_id=1,
                body="Test data for body",
                favorite_id=1,
                account_id=2,
            )


def fake_current_account_data():
    return {
        "account_id": 2,
        "username": "user",
        "city": 98765
    }


def test_create_note():
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_current_account_data
    app.dependency_overrides[NotesQueries] = MockNotesQueries
    favorite_id = 1

    fake_data = {
        "body": "Test data for body",
        "favorite_id": 1,
        "account_id": 2
        }
    response = client.post(f"/favorites/{favorite_id}/notes/", json=fake_data)
    assert response.status_code == 200
    assert response.json() == {
        "note_id": 1,
        "body": "Test data for body",
        "favorite_id": 1,
        "account_id": 2,
    }

    app.dependency_overrides = {}
