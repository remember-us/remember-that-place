from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.favorites import FavoritesQueries

client = TestClient(app)


class MockFavoritesQueries:
    def get_favorite(self, favorite_id: int, account_id: int):
        return {
            "favorite_id": 5,
            "name": "Fake Name",
            "image_url": "fake.com",
            "reference_id": "fakeref",
            "account_id": 5,
        }


def fake_favorites_data():
    return {
        "favorite_id": 5,
        "name": "Fake Name",
        "image_url": "fake.com",
        "reference_id": "fakeref",
        "account_id": 5,
    }


def test_get_favorite():
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_favorites_data

    app.dependency_overrides[FavoritesQueries] = MockFavoritesQueries
    favorite_id = 5
    response = client.get(f"/favorites/{favorite_id}")
    assert response.status_code == 200
    assert response.json() == {
        "favorite_id": 5,
        "name": "Fake Name",
        "image_url": "fake.com",
        "reference_id": "fakeref",
        "account_id": 5,
    }

    app.dependency_overrides = {}
