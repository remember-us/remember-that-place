from fastapi.testclient import TestClient
from main import app
from queries.notes import NotesQueries, NoteOut
from authenticator import authenticator

client = TestClient(app)


class MockNotesQueries:
    def get_note(self, note_id: int, account_id: int, favorite_id: int):
        return NoteOut(
            note_id=note_id,
            account_id=account_id,
            favorite_id=favorite_id,
            body="New go-to burger place!"
        )


def fake_current_account_data():
    return {
        "account_id": 2,
        "username": "user",
        "city": "NYC"
    }


def test_get_note():

    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_current_account_data
    app.dependency_overrides[NotesQueries] = MockNotesQueries
    favorite_id = 3
    note_id = 1

    response = client.get(f"/favorites/{favorite_id}/notes/{note_id}")

    assert response.status_code == 200
    assert response.json() == {
        "note_id": 1,
        "account_id": 2,
        "favorite_id": 3,
        "body": "New go-to burger place!"
    }

    app.dependency_overrides = {}
