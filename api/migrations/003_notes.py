steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE notes (
            note_id INT GENERATED ALWAYS AS IDENTITY,
            body VARCHAR(255) NOT NULL,
            account_id INT NOT NULL,
            favorite_id INT NOT NULL,
            PRIMARY KEY(note_id),
            FOREIGN KEY(favorite_id) REFERENCES favorites(favorite_id) ON DELETE CASCADE,
            FOREIGN KEY(account_id) REFERENCES accounts(account_id) ON DELETE CASCADE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE notes;
        """,
    ]
]
