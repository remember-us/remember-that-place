steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE favorites (
            favorite_id INT GENERATED ALWAYS AS IDENTITY,
            name VARCHAR(255) NOT NULL,
            image_url VARCHAR(255) NOT NULL,
            reference_id VARCHAR(255) NOT NULL,
            account_id INT NOT NULL,
            PRIMARY KEY(favorite_id),
            FOREIGN KEY(account_id) REFERENCES accounts(account_id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE favorites;
        """,
    ]
]
