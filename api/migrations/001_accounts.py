steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE accounts (
            account_id INT GENERATED ALWAYS AS IDENTITY,
            username VARCHAR(20) NOT NULL,
            city VARCHAR(25) NOT NULL,
            hashed_password VARCHAR(255) NOT NULL,
            PRIMARY KEY(account_id),
            UNIQUE (username)
        );
        """,
        """
        DROP TABLE accounts;
        """,
    ]
]
