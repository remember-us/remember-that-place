## Accounts

### Get Account

-   Endpoint path: /accounts/{username}
-   Endpoint method: GET
-   Query parameters:

    -   username: the username of the account to be retrieved

-   Headers:

    -   Authorization: Bearer token

-   Response: account information
-   Response shape (JSON):
    ```json
    {
        "account_id": int,
        "username": string,
        "city": string
    }
    ```

### Create Account

-   Endpoint path: /accounts
-   Endpoint method: POST

-   Request shape (JSON):

    ```json
    {
        "username": string,
        "password": string,
        "city": string
    }
    ```

-   Response: created token and account information
-   Response shape (JSON):
    ```json
    {
        "access_token": string,
        "token_type": "Bearer",
        "account": {
            "account_id": int,
            "username": string,
            "city": string
        }
    }
    ```

### Update Account

-   Endpoint path: /accounts/{account_id}
-   Endpoint method: PUT

-   Headers:

    -   Authorization: Bearer token

-   Request shape (JSON):

    ```json
    {
        "username": string,
        "password": string,
        "city": string
    }
    ```

-   Response: updated account information (except password)
-   Response shape (JSON):
    ```json
    {
        "account_id": int,
        "username": string,
        "city": string
    }
    ```

## Auth

### Get Token

-   Endpoint path: /token
-   Endpoint method: GET

-   Headers:

    -   Authorization: Bearer token

-   Response: token information for the current login session
-   Response shape (JSON):
    ```json
    {
        "access_token": string,
        "token_type": "Bearer",
        "account": {
            "account_id": int,
            "username": string,
            "city": string
        }
    }
    ```

### Login

-   Endpoint path: /token
-   Endpoint method: POST

-   Request shape (JSON):

    ```json
    {
        "username": string,
        "password": string,
    }
    ```

-   Response: token information for the new login session
-   Response shape (JSON):
    ```json
    {
        "access_token": string,
        "token_type": "Bearer"
    }
    ```

### Logout

-   Endpoint path: /token
-   Endpoint method: DELETE

-   Headers:

    -   Authorization: Bearer token

-   Response: always true
-   Response shape (JSON):
    ```json
    true
    ```

## Favorites

### Get All Favorites

-   Endpoint path: /favorites
-   Endpoint method: GET

-   Headers:

    -   Authorization: Bearer token

-   Response: a list of favorites to the current logged in account
-   Response shape (JSON):
-   ```json
    [
        {
            "favorite_id": 10,
            "name": "886",
            "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/N-V-Dl4nrmMhN1TcVt6a8g/o.jpg",
            "account_id": 5,
            "reference_id": "886-new-york"
        },
        {
            "favorite_id": 11,
            "name": "Old National Discount Mall",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/D8OUPrkxeTHOOnnO2OLcOg/o.jpg",
            "account_id": 5,
            "reference_id": "old-national-discount-mall-college-park"
        },
        {
            "favorite_id": 12,
            "name": "Habibi Rooftop the Restaurant",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/GlSu0X-h9R7OdiApuNcMIQ/o.jpg",
            "account_id": 5,
            "reference_id": "habibi-rooftop-the-restaurant-brooklyn"
        },
        {
            "favorite_id": 13,
            "name": "Raku",
            "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/GEbZnH1jR4n7D5WOJ3urPg/o.jpg",
            "account_id": 5,
            "reference_id": "raku-new-york-7"
        }
    ]
    ```

### Get Favorite

-   Endpoint path: /favorites/{favorite_id}
-   Endpoint method: GET

-   Headers:

    -   Authorization: Bearer token

-   Response: a single favorite
    to the current logged in account
-   Response shape (JSON):
-   ```json
    [
        {
            "favorite_id": 1,
            "name": "Sobak",
            "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/jIzMOv8F8L5Yk1E-1KYQtg/o.jpg",
            "account_id": 1,
            "reference_id": "sobak-new-york-2"
        }
    ]
    ```

### Create a Favorite

-   Endpoint path: /favorites
-   Endpoint method: POST

-   Headers:

    -   Authorization: Bearer token

-   Request: The information of the favorite
-   Request Shape (JSON):
-   ```json
    {
        "name": "cool place",
        "image_url": "place.com",
        "reference_id": "Hdleit8e900766"
    }
    ```

-   Response: The created favorite or a error response
-   Response shape (JSON):
-   ```json
    {
        "favorite_id": 1,
        "name": "cool place",
        "image_url": "place.com",
        "account_id": 1,
        "reference_id": "Hdleit8e900766"
    }
    ```

### Delete Favorite

-   Endpoint path: /favorites/{favorite_id}
-   Endpoint method: DELETE

-   Headers:

    -   Authorization: Bearer token

-   Response: a boolean value or error
-   Response shape (bool value):
-   ```json
    true
    ```

## Notes

### Get Notes

-   Endpoint path: /favorites/{favorite_id}/notes
-   Endpoint method: GET

-   Headers:

    -   Authorization: Bearer token
    Request: The information of the favorites_id and account_id
-   Request Shape (JSON):
-   ```json
    {
        "favorite_id": "6",
        "account_id": "5",
    }
    ```
-   Response: a list of notes per favorite_id to the current logged in account
-   Response shape (JSON):
-   ```json
    [
        {
            "note_id": 1,
            "body": "test note",
            "favorite_id": 6,
            "account_id": 5
        }
    ]
    ```

### Create a Note

-   Endpoint path: /favorites/{favorite_id}/notes
-   Endpoint method: POST

-   Headers:

    -   Authorization: Bearer token

-   Request: The information of the note
-   Request Shape (JSON):
-   ```json
    {
        "body": "test note",
        "account_id": 5,
        "favorite_id": 6
    }
    ```

-   Response: The created note or a error response
-   Response shape (JSON):
-   ```json
    {
        "note_id": 1,
        "body": "test note",
        "favorite_id": 6,
        "account_id": 5
    }
    ```

### Get a note

-   Endpoint path: /favorites/{favorite_id}/notes{note_id}
-   Endpoint method: GET

-   Headers:

    -   Authorization: Bearer token

-   Response: a single note to the current logged in account
-   Response shape (JSON):
-   ```json
    {
        "note_id": 1,
        "body": "test note",
        "favorite_id": 6,
        "account_id": 5
    }
    ```


### Delete Note

-   Endpoint path: /favorites/{favorite_id}/notes/{note_id}
-   Endpoint method: DELETE

-   Headers:

    -   Authorization: Bearer token

-   Response: a boolean value or error
-   Response shape (bool value):
-   ```json
    true
    ```

### Update Note

-   Endpoint path: /favorites/{favorite_id}/notes/{note_id}
-   Endpoint method: PUT

-   Headers:

    -   Authorization: Bearer token
    
-   Request: The information of the note
-   Request Shape (JSON):
-   ```json
    {
        "body": "updated note",
        "account_id": 5,
        "favorite_id": 6
    }
    ```

-   Response: an updated note to the current logged in account
-    Response shape (JSON):
-   ```json
    {
        "note_id": 1,
        "body": "updated note",
        "favorite_id": 6,
        "account_id": 5
    }
    ```
