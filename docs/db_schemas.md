# Database Tables

Below is a collection of markdown tables that illustrate the layout of our database

#### Table of Contents

-   [Database Tables](#database-tables) - [Table of Contents](#table-of-contents)
    -   [Accounts](#accounts)
    -   [Favorites](#favorites)
    -   [Notes](#notes)

## Accounts

This is a database table for a user. The `city` is used to display locations in the same city as the user.

| Name            | Type    | Maximum length | Primary key | Required |
| --------------- | ------- | -------------- | ----------- | -------- |
| account_id      | INT     | N/A            | yes         | N/A      |
| username        | VARCHAR | 20             | no          | yes      |
| city            | VARCHAR | 25             | no          | yes      |
| hashed_password | VARCHAR | 255            | no          | yes      |

---

## Favorites

This is a database table for a favorited location, the `reference_id` is used to store the id
of a location provided from the YelpAPI and is used to call the location from the YelpAPI.
The `account_id` use used to tie this favorite to the account that created it.

| Name                     | Type    | Maximum length | Primary key | Required |
| ------------------------ | ------- | -------------- | ----------- | -------- |
| favorite_id              | INT     | N/A            | yes         | N/A      |
| name                     | VARCHAR | 255            | no          | yes      |
| image_url                | VARCHAR | 255            | no          | yes      |
| reference_id             | VARCHAR | 255            | no          | yes      |
| account id `accounts FK` | INT     | N/A            | no          | yes      |

---

## Notes

This is a database table for a note. The `account_id` is used to prevent other users from seeing notes that dont match their `account_id`, the `favorites_id` is used to display the note alongside a specific favorite.

| Name                       | Type    | Maximum length | Primary key | Required |
| -------------------------- | ------- | -------------- | ----------- | -------- |
| note_id                    | INT     | N/A            | yes         | N/A      |
| body                       | VARCHAR | 255            | no          | yes      |
| account_id `accounts FK`   | INT     | N/A            | no          | yes      |
| favorite_id `favorites FK` | INT     | N/A            | no          | yes      |
