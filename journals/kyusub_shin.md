2/9/24

-   Worked on the readme and as a team, completed the readme and documentation

2/8/24

-   The entire team:

1. Fixed the frontend authentication so the user can log in and out successfully on the website
2. Built the location details page and created the route to it via React Router

2/6/24

-   Added styling to signup and login pages
-   Added user logout functionality and added logout button to navigation bar
-   Updated the navigation bar so that it displays buttons based on whether the user is logged in

2/5/24

-   Built the signup page and added frontend authentication to it
-   Added the React route to the signup page

2/1/24

-   In our pair:

1. Built the login page with frontend authentication integrated to it
2. Added a route to the login page to the App and navigation bar

1/31/24

-   The whole team successfully set up Bootstrap to work in our project by modifying several files in the ghi directory, as well as doing online research on Bootstrap and troubleshooting
-   The whole team started working on the navigation bar
-   Split into pairs and started working on the login and signup pages

1/30/24

-   The entire team started on the frontend portion of the project by:
    1. Checking that JSX content is showing up on the website
    2. Starting on the implementation of the navigation bar

1/29/24

-   The entire team:
    1. Tested and troubleshooted the routes and queries for getting, creating, updating, and deleting notes associated with favorited locations
    2. Tested and troubleshooted the routes and queries for getting and creating favorited locations
    3. Updated all routes and queries to adhere to RESTful conventions
-   Particular lessons learned:
    1. How to create multiple foreign keys in a PostgreSQL table

1/25/24

-   Split up into pairs and completed the following:
    1. Added routes for editing and deleting a note for a favorited location
    2. Worked on and troubleshooted the "get token" functionality for the authentication to work properly

1/24/24

-   Split up into pairs and completed the following for the notes model:
    1. Created routes for getting and creating a note for a favorited location
    2. Created queries for getting, creating, editing, and deleting a note for a favorited location
-   Also created a route for getting an existing account by providing a username

1/23/24

-   Split up into pairs and continued working on the backend authentication with Blake. Finished the day by configuring token expiration management

1/22/24

-   Worked on backend authentication with team, with me being the "driver". Unable to bring up the web appliation with "docker compose up" command and kept troubleshooting it

1/19/24

-   Collaborated with team to:
    1. Design our models using Excalidraw
    2. Create the tables for our models for the database

1/18/24

-   Created my own git branch and merged it with the main branch, which has the database setup completed. Implemented the backend authentication with team, with Blake being the driver. At the end of the day, Blake merged his branch, which has the backend auth code, with the main branch. I merged my branch with main

1/17/24

-   Worked on Postgres database setup with team, with Sean being the driver. Spent most of the day troubleshooting an issue regarding pulling of data from the .env file to connect to the Postgres server, which was causing the FastAPI container to be down and show errors

1/16/24

-   Modified Docker Compose file and created .env file (under ghi folder) to successfully bring up the web app
