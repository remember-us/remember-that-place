## Dev Diary

This is a Dev journal to track my progress and notes while working on the Remember That Place project. Usually I'll add a time stamp but if I dont thats typically a summary of the day

<details>
<summary>Table of Contents</summary>

[TOC]

</details>

---

## 2/9

It was a time crunch to ensure everything was done on time but we completed all of the necesary components of our MVP! Its submittion day!

## 2/6

Worked on the carousel with blake and changed all the mentions of "zipcode" too "city" because processing a zipcode would prevent us from finnishing on time. Today was rough because we have/had alot of front end to do but we got alot of it done and were feeling pretty good.

## 2/5

Worked with the team to create front end components, we worked on the signin/login page, carousels for location displays ect.

## 1/30

Finalized making the back end routes more restful and started working on the back end

## 1/29

We worked as a single group to make the back end components more restful

## 1/25-26

Worked with sean again to finalize Creating a favorite but was unable to get the favorites unfortunately.
The rest of the back end asides from favorites is finnished so far, after we complete favorties we should be able to
move to the front end!

## 1/23-24

Worked together with sean to add the Yelp API into our project, we split into two pairs, sean and i worked on locations and favorites, blake and kyusub worked on accounts and notes. I drove for most of the Yelp API portion and sean is going to start driving for the favorites integration tommarow.

### 1/22

Today we tackled working on the routes to create an account and we have been encountering alot of problems.
We did our best to get what we could done but we didnt get as far as we would have prefered. Kyusub drove for the day so we could get him some commits in.

### 1/19

Helped implement authentication using JWT. Started building out API endpoints for core features.
Blake Drove for the start of the day, we made a lot of progress on auth and API scaffolding. ~11:30

We completed JWT auth and we used liveshare to collaborate on the creation of the database tables and created a new PG-data volume, we also edited the docker compose to add a signing key for everyone due to difficulties we faced durring the day today. - 7:43

### 1/18

Finnished and merged the database, We are now starting to impliment authentication for users and user accounts. once we finnishe that we are going to start building out the back end funcitonality for the core app features. - 3:42 pm

### 1/17

Set up the database and created the docker compose.

### 1/16

Initial set up, cloning project and gearing up.

#### Goals

-   [x] Design Authentication and User accounts.
