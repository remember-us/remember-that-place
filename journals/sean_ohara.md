# Project Journal for [Sean O'Hara](@seanoharadev)

<details>
<summary>Table of Contents</summary>

[TOC]

</details>

## Friday, Feb 9, 2024

Finally fixed the issues I was having with git worktrees yesterday. Turns out, the problem was actually me. I was using it wrong, and made a wrong assumption on what was happening. Finishing things up with the team today.

---

Unable to locate missing Journal entries. I believe I deleted them by mistake in frustration with the git worktrees.

---

## Monday, Jan 29, 2024

Back to working as just one large group, we all worked on finishing up with the backend.

## Friday, Jan 26, 2024

Worked again on finishing up with the favorites. Almost done with the project backend now.

## Thursday, Jan 25, 2024

Same groups again, but this time we worked on getting the favorites portion setup and functional.

## Wednesday, Jan 24, 2024

Still working in the same groups, me and Xetta finished up getting the Yelp API portion working.

## Tuesday, Jan 23, 2024

Today we decided to split into groups. Kyusub and Blake were one group, me an Xetta the other. We worked on getting the Yelp API integration setup and functional.

## Monday, Jan 22, 2024

Today Kyusub drove while the team worked on Authentication. We kept having issues getting a token. This drove us crazy trying to figure out why. Also, I have decided not to follow the journal template I came up with. It just takes too long to format considering everything else I've got going on.

## Friday, Jan 19, 2024

Team collaboration day. Xetta and Blake drove for different parts of the day. The entire team collaborated on getting the PostgreSQL database finally populated with some tables for our project. We all also communicated how we were feeling about where we currently are in our understanding and abilities.

### Goals

-   [ ] Get PostgreSQL database populated with tables
-   [ ] Finish refactor of prior journal entries

### Accomplishments

-   [x] Get PostgreSQL database populated with tables
-   [x] Fixed prior journal entries
-   [x] Setup and got git worktree setup for the project

### Challenges

-   Issues getting PostgreSQL tables setup.
    -   Further issues trying to understand why couldn't remove the "sample" migration file.
-   Issues with git worktree.
    -   Difference between --bare and --mirror unclear at start, and caused issues being able to push, pull, fetch, and deal with any updates when using --mirror as a result.

### Growth

-   I feel we all finally started understanding the PostgreSQL database table setup today.

### Reflection

-   Personally, I believe that we all can work well together. As a team, we might need to work a little more on our Ubiquitous language to make sure we all understand each other.

### Brainstorming

-   Need to work on getting overall documentation written for the project.
    -   Setup a wiki for the project
-   Looking into Atlassian items to be used and learned/worked with as well.
    -   Jira, Jira Work Management, Jira Product Discovery, Confluence, etc
-   At least need to get all of the settings, tools, and extensions listed out in rough form
    -   Only need a basic list to at least lead the group conversation with.

---

## Thursday, Jan 18, 2024

Randomness goes here

### Goals

-   [ ] Resolve PostgreSQL setup errors
-   [ ] Update a few QoL items
-   [ ] Refactor journal entry to fix date layout

### Accomplishments

-   [x] Resolve PostgreSQL setup errors
-   [x] Update a few QoL items
    -   [x] Create entry_sample.md

### Challenges

-   Didn't get to finish Journal, and currently do not specifically remember the issues. There were several, but we did manage to work thru them all between 1/18 and 1/19.

### Growth

-   After reviewing lots of documentation, finally figured out why we couldn't get PostgreSQL database fully configured, and also why unable to get the .env variables to be properly read and input into the docker-compose.yaml file. Definitely feel that I understand and have a better understanding of docker and docker-compose now.

### Reflection

-   Need to work on time management to make sure to get to journal, goals, and brainstorming each day

---

## Wednesday, Jan 17, 2024

Setting up base _skeleton_ for project documentation in _docs/_ [^2]

| Status           |                  File                  |                                          Reason |
| :--------------- | :------------------------------------: | ----------------------------------------------: |
| Updated & Linked |            _docs/README.MD_            | Linked index and brief description of docs [^1] |
| Created          | _docs/appendix/1.vscode-extensions.md_ |                  Team VS Code Extensions in use |
| Created          |  _docs/appendix/2.vscode-settings.md_  |                    Team VS Code Settings in use |
| Created          |  _docs/developer/1.initial-setup.md_   |                Initial setup for local dev work |
| Created          |    _docs/developer/2.branching.md_     |       Team conventions for everything branching |
| Created          |  _docs/developer/3.merge-requests.md_  |             Team conventions for merge requests |
| Created          |   _docs/developer/4.code-review.md_    |                Team conventions for code review |

Also updating Markdown formatting on this file to have an example to speak with the team on conventions and options available for our project. [^s1]

#### Errors

While getting PostgreSQL initially setup, along with pg-admin, ran into multiple errors.

-   Many issues with the docker-compose.yaml file, and not wanting to pull variables from .env file.
-   Issues with pgadmin not being able to connect to the database.
-   Issues with fastapi not being able to connect to the database.
-   Issues with fastapi crashing when trying to connect to the database.

---

## Tuesday, Jan 16, 2024

Ran multiple tests all relating to the docker-compose.yaml file using
the "user" option. After reading lots of documentation online, and all
the testing, I will instead be testing a few things and speaking with the
team tomorrow. As best I can tell at this time, we should not have any
need to use the option.

Created a base example env file for vite

-   **ghi/.env.example**

Setup actual **.env** file

Reviewed all project files, code, and documentation.

Completed initial setup of the project for local dev work.

[^s1]: Items including the footnote tags are being tested to verify how fully functions.
[^1]: Should probably rename to _docs/index.md_
[^2]:
    Mostly being verbose to showcase some convention ideas
    es setup.

    -   Further issues trying to understand why couldn't remove the "sample" migration file.

-   Issues with git worktree.
    -   Difference between --bare and --mirror unclear at start, and caused issues being able to push, pull, fetch, and deal with any updates when using --mirror as a result.

### Growth

-   I feel we all finally started understanding the PostgreSQL database table setup today.

### Reflection

-   Personally, I believe that we all can work well together. As a team, we might need to work a little more on our Ubiquitous language to make sure we all understand each other.

### Brainstorming

-   Need to work on getting overall documentation written for the project.
    -   Setup a wiki for the project
-   Looking into Atlassian items to be used and learned/worked with as well.
    -   Jira, Jira Work Management, Jira Product Discovery, Confluence, etc
-   At least need to get all of the settings, tools, and extensions listed out in rough form
    -   Only need a basic list to at least lead the group conversation with.

---

## Thursday, Jan 18, 2024

Randomness goes here

### Goals

-   [ ] Resolve PostgreSQL setup errors
-   [ ] Update a few QoL items
-   [ ] Refactor journal entry to fix date layout

### Accomplishments

-   [x] Resolve PostgreSQL setup errors
-   [x] Update a few QoL items
    -   [x] Create entry_sample.md

### Challenges

-   Didn't get to finish Journal, and currently do not specifically remember the issues. There were several, but we did manage to work thru them all between 1/18 and 1/19.

### Growth

-   After reviewing lots of documentation, finally figured out why we couldn't get PostgreSQL database fully configured, and also why unable to get the .env variables to be properly read and input into the docker-compose.yaml file. Definitely feel that I understand and have a better understanding of docker and docker-compose now.

### Reflection

-   Need to work on time management to make sure to get to journal, goals, and brainstorming each day

---

## Wednesday, Jan 17, 2024

Setting up base _skeleton_ for project documentation in _docs/_ [^2]

| Status           |                  File                  |                                          Reason |
| :--------------- | :------------------------------------: | ----------------------------------------------: |
| Updated & Linked |            _docs/README.MD_            | Linked index and brief description of docs [^1] |
| Created          | _docs/appendix/1.vscode-extensions.md_ |                  Team VS Code Extensions in use |
| Created          |  _docs/appendix/2.vscode-settings.md_  |                    Team VS Code Settings in use |
| Created          |  _docs/developer/1.initial-setup.md_   |                Initial setup for local dev work |
| Created          |    _docs/developer/2.branching.md_     |       Team conventions for everything branching |
| Created          |  _docs/developer/3.merge-requests.md_  |             Team conventions for merge requests |
| Created          |   _docs/developer/4.code-review.md_    |                Team conventions for code review |

Also updating Markdown formatting on this file to have an example to speak with the team on conventions and options available for our project. [^s1]

#### Errors

While getting PostgreSQL initially setup, along with pg-admin, ran into multiple errors.

-   Many issues with the docker-compose.yaml file, and not wanting to pull variables from .env file.
-   Issues with pgadmin not being able to connect to the database.
-   Issues with fastapi not being able to connect to the database.
-   Issues with fastapi crashing when trying to connect to the database.

---

## Tuesday, Jan 16, 2024

Ran multiple tests all relating to the docker-compose.yaml file using
the "user" option. After reading lots of documentation online, and all
the testing, I will instead be testing a few things and speaking with the
team tomorrow. As best I can tell at this time, we should not have any
need to use the option.

Created a base example env file for vite

-   **ghi/.env.example**

Setup actual **.env** file

Reviewed all project files, code, and documentation.

Completed initial setup of the project for local dev work.

[^s1]: Items including the footnote tags are being tested to verify how fully functions.
[^1]: Should probably rename to _docs/index.md_
[^2]: Mostly being verbose to showcase some convention ideas
