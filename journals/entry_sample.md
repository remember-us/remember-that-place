## Time Frame Template

Date of entry, and any basic notes/quick blurbs

### Goals

-   [ ] Goals to accomplish
-   [~] Should include any goals that are still in progress
    -   [ ] Also sub goals that are needed for parent goal

### Accomplishments

-   [x] Goals that were accomplished
-   [x] Including any unknown goals that were accomplished
-   [x] Don't remove items from Goals section above
    -   [x] This is for what is accomplished
        -   [~] Goals are what the plan is to accomplish

### Challenges

-   Any challenges that came up while working.
-   Including any that were also resolved.

### Growth

-   Quick synopsis of any personal growth/learning that occurred during the time frame.

### Reflection

-   Overall reflection for the work completed for the time frame.

### Brainstorming

-   Personal brainstorming notes.
    -   May happen at any time during the time frame.
    -   Can include anything that comes up from group meetings.

---
